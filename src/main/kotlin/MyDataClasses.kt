package git2pg_kt

import org.eclipse.jgit.diff.DiffEntry

data class MyEditListInfo(
    val fileType: MyFileType,
    override val insert: Int,
    override val delete: Int,
    override val replace: Int
) : MyDiff

data class MyEditInfo(
    override val insert: Int,
    override val delete: Int,
    override val replace: Int
) : MyDiff

data class MyModInfo(
    val commitId: String,
    val modType: DiffEntry.ChangeType?,
    val fileType: MyFileType,
    val oldId: String,
    val newId: String,
    val oldPath: String,
    val newPath: String,
    val lineCount: Int,
    val byteCount: Long,
    val diff: MyDiff
)

data class MyUser(
    val name: String,
    val email: String
)

data class MyTag(
    val commitId: String,
    val tagName: String,
    val tagMessage: String
)

data class MyNote(
    val commitId: String,
    val note: String
)

data class MyBranchCommit(
    val name: String,
    val commitId: String
)