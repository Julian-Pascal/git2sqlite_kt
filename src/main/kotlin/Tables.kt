package git2pg_kt

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.jodatime.datetime

object Commits : Table() {
    val commitid = varchar("commitid", 40).primaryKey() // Column<String>
    val authorts = datetime("authorts")
    val author = integer("author").references(Users.id)
    val committerts = datetime("committerts")
    val committer = integer("committer").references(Users.id)
    val message = text("message")
}

object Users : Table() {
    val id = integer("id").primaryKey()
    val name = varchar("name", length = 255)
    val email = varchar("email", length = 255)
}

object Parents : Table() {
    val child = varchar("child", 40).primaryKey(0).references(Commits.commitid)
    val parent = varchar("parent", 40).primaryKey(1).references(Commits.commitid)
}

object Branches : Table() {
    val commitid = varchar("commitid", 40).primaryKey(0).references(Commits.commitid)
    val branch = varchar("branch",255).primaryKey(1)
}

object Tags : Table() {
    val tag = varchar("tag", 255).primaryKey()
    val commitid = varchar("commitid",40).references(Commits.commitid)
    val message = text("message")
}

object Filemods : Table() {
    val id = integer("id").autoIncrement().primaryKey()
    val commitid = varchar("commitid", 40).references(Commits.commitid)
    val modtype = varchar("modtype", 10)
    val filetype = varchar("filetype", 10)
    val oldid = varchar("oldid", 40)
    val newid = varchar("newid", 40)
    val oldpath = text("oldpath")
    val newpath = text("newpath")
    val linecnt = integer("linecnt")
    val bytecnt = long("bytecnt")
    // diff
    val insertcnt = integer("insertcnt")
    val deletecnt = integer("deletecnt")
    val replacecnt = integer("replacecnt")
}

object Notes : Table() {
    val id = integer("id").autoIncrement().primaryKey()
    val commitid = varchar("commitid", 40).references(Commits.commitid)
    val note = text("note")
}