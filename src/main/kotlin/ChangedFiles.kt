package git2pg_kt

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.diff.*
import org.eclipse.jgit.errors.MissingObjectException
import org.eclipse.jgit.lib.ObjectId
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.treewalk.TreeWalk
import org.eclipse.jgit.treewalk.filter.PathFilter
import org.eclipse.jgit.util.io.DisabledOutputStream
import kotlin.sequences.Sequence

object CommitInfoExt {

    /*fun commitInfoExt(git: Git, commit: RevCommit) : Sequence<MyModInfo>? {
        val modInfo = changedFilesWithDiff(git, commit)
        //print(".")
        return modInfo
    }*/

    fun modificationInfo(git: Git, commit: RevCommit, diffEntry: DiffEntry, diffFormatter: DiffFormatter): MyModInfo {
        val diff = diffInfo(diffEntry, diffFormatter)
        val fileType = diff.fileType
        val modType = modType(diffEntry)
        val newPath = diffEntry.newPath
        return MyModInfo(
            commitId = commit.name,
            modType = modType,
            fileType = fileType,
            oldId = diffEntry.oldId.name(),
            newId = diffEntry.newId.name(),
            oldPath = diffEntry.oldPath,
            newPath = newPath,
            lineCount = when {
                modType == DiffEntry.ChangeType.DELETE || fileType == MyFileType.Bin -> 0
                else -> linecntOfFileFromCommit(git = git, commit = commit, pathname = newPath)
            },
            byteCount = when (modType) {
                DiffEntry.ChangeType.DELETE -> 0
                else -> bytecntOfFileFromCommit(git = git, commit = commit, pathname = newPath)
            },
            diff = diff
        )
    }

    fun changedFilesWithDiff(git: Git, commit: RevCommit): Sequence<MyModInfo>? {
        val diffFormatter = diffFormatterForChanges(git)
        return getDiffEntries(commit, diffFormatter)?.map { diffEntry ->
            modificationInfo(git, commit, diffEntry, diffFormatter)
        }
    }

    /* Reports the type and count of changes of the given `diff-entry`.
    Returns MyEditListInfo{fileType: Txt, insert: Int, delete: Int, replace: Int},
    or {fileType: Bin, insert: 0, delete: 0, replace: 0} if the file is binary.*/
    fun diffInfo(diffEntry: DiffEntry, diffFormatter: DiffFormatter): MyEditListInfo {
        return editListInfo(diffFormatter.toFileHeader(diffEntry).toEditList())
    }

    fun modType(diffEntry: DiffEntry): DiffEntry.ChangeType? {
        return diffEntry.changeType
    }

    fun editListInfo(editList: EditList): MyEditListInfo {
        if (editList.isEmpty()) {
            return MyEditListInfo(MyFileType.Bin, 0, 0, 0)
        }
        val eiList = editList.map { editInfo(it) }
        val eiSum = eiList.reduce { ei1, ei2 -> addEditInfo(ei1, ei2) }
        val eli = MyEditListInfo(MyFileType.Txt, eiSum.insert, eiSum.delete, eiSum.replace)
        return eli
    }

    /**
     * ToDo: optimisation?
     */
    fun editInfo(edit: Edit): MyEditInfo {
        val lenOld = maxOf(edit.lengthA, 0)
        val lenNew = maxOf(edit.lengthB, 0)
        val i = maxOf(lenNew - lenOld, 0)
        val d = maxOf(lenOld - lenNew, 0)
        val r = minOf(lenOld, lenNew)
        return MyEditInfo(insert = i, delete = d, replace = r)
    }

    fun addEditInfo(ei1: MyEditInfo, ei2: MyEditInfo): MyEditInfo {
        return MyEditInfo(
            insert = ei1.insert + ei2.insert,
            delete = ei1.delete + ei2.delete,
            replace = ei1.replace + ei2.replace
        )
    }

    fun objectIdFromCommit(git: Git, commit: RevCommit, pathname: String): ObjectId? {
        val tree = commit.tree
        val tw: TreeWalk = TreeWalk(git.repository).use {
            it.addTree(tree)
            it.isRecursive = true
            it.filter = PathFilter.create(pathname)
            it
        }
        return when {
            tw.next().not() -> null
            else -> tw.getObjectId(0)
        }
    }

    fun linecntOfFileFromCommit(git: Git, commit: RevCommit, pathname: String): Int {
        val diffFormatter = diffFormatterForChanges(git)
        val diffEntry = diffFormatter.scan(null, commit).single { it.newPath == pathname }
        return diffInfo(diffEntry, diffFormatter).insert
    }

    fun bytecntOfFileFromCommit(git: Git, commit: RevCommit, pathname: String): Long {
        val objectId = objectIdFromCommit(git, commit, pathname)
        return try {
            git.repository.open(objectId).size
        } catch (e: MissingObjectException) {
            0
        }
    }

    fun diffFormatterForChanges(git: Git): DiffFormatter {
        DiffFormatter(DisabledOutputStream.INSTANCE).use { f ->
            f.setRepository(git.repository)
            f.setDiffComparator(RawTextComparator.DEFAULT)
            f.isDetectRenames = true
            return f
        }
    }

    fun changedFiles(git: Git, commit: RevCommit): Sequence<String?>? {
        val diffFormatter = diffFormatterForChanges(git)
        return getDiffEntries(commit, diffFormatter)?.map { this.changedFileInfo(it) }
    }

    fun getDiffEntries(commit: RevCommit, diffFormatter: DiffFormatter): Sequence<DiffEntry>? {
        //Does it work if parentCount is zero?
        val parent: RevCommit?
        when {
            commit.parentCount <= 0 -> parent = null
            else -> parent = commit.parents.first()
        }
        return diffFormatter.scan(parent, commit).asSequence()
    }

    fun changedFileInfo(entry: DiffEntry): String? {
        val oldPath = entry.oldPath
        val newPath = entry.newPath
        return when {
            oldPath == newPath -> newPath
            oldPath == "dev/null" -> newPath
            newPath == "dev/null" -> oldPath
            else -> oldPath
        }
    }
}
