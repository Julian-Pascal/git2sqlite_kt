package git2pg_kt

import org.eclipse.jgit.lib.PersonIdent
import org.eclipse.jgit.revwalk.RevCommit
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.insert
import org.joda.time.DateTime

object Extract {
    fun insertCommits(revWalk: Iterable<RevCommit>) {
        SchemaUtils.create(Commits)
        println("start insertCommits")
        //commit insert
        Commits.batchInsert(revWalk) { commit ->
            this[Commits.commitid] = commit.name
            this[Commits.authorts] = DateTime(commit.authorIdent.`when`)
            this[Commits.author] = getUserId(commit.authorIdent)
            this[Commits.committerts] = DateTime(commit.committerIdent.`when`)
            this[Commits.committer] = getUserId(commit.committerIdent)
            this[Commits.message] = commit.fullMessage
        }
        println("commits ... done")

        println("insertCommits done")
    }

    private fun flatMapChildrenParents(children: Iterable<RevCommit>): Set<Pair<String, String>> {
        fun mapChildParents(child: RevCommit): List<Pair<String, String>> {
            return child.parents.map { parent: RevCommit ->
                Pair(child.name, parent.name)
            }
        }

        return children.filterNot { it.parents.isEmpty() }.map { mapChildParents(it) }.flatten().toSet()
    }

    fun insertBranches(branchName_commit_pairs: List<Pair<String, Iterable<RevCommit>>>) {
        SchemaUtils.create(Branches)
        for (bncPair in branchName_commit_pairs) {
            val branchName = bncPair.first
            println("Branch: $branchName")
            val commits = bncPair.second

            commits.forEach { commit ->
                Branches.insert {
                    it[commitid] = commit.name
                    it[branch] = branchName
                }
            }
        }
    }

    fun insertBranch(branchCommitList: List<MyBranchCommit>) {
        SchemaUtils.create(Branches)
        Branches.batchInsert(branchCommitList) {br ->
            this[Branches.commitid] = br.commitId
            this[Branches.branch] = br.name
        }
        print("b")
    }

    fun insertTags(tags: Iterable<MyTag>) {
        SchemaUtils.create(Tags)

        Tags.batchInsert(tags) { tg ->
            this[Tags.commitid] = tg.commitId
            this[Tags.tag] = tg.tagName
            this[Tags.message] = tg.tagMessage
        }
    }

    fun insertParents(revWalk: Iterable<RevCommit>) {
        SchemaUtils.create(Parents)
        val childrenParents = flatMapChildrenParents(revWalk)
        Parents.batchInsert(childrenParents) { cp ->
            this[Parents.child] = cp.first //child bezieht sich auf das commit selbst
            this[Parents.parent] = cp.second
        }
        println("parents ... done")
    }

    fun insertFilemods(modInfos: Iterable<MyModInfo>) {
        SchemaUtils.create(Filemods)
        Filemods.batchInsert(modInfos) { fm ->
            this[Filemods.commitid] = fm.commitId
            this[Filemods.modtype] = fm.modType!!.name
            this[Filemods.filetype] = fm.fileType.name
            this[Filemods.oldid] = fm.oldId
            this[Filemods.newid] = fm.newId
            this[Filemods.oldpath] = fm.oldPath
            this[Filemods.newpath] = fm.newPath
            this[Filemods.linecnt] = fm.lineCount
            this[Filemods.bytecnt] = fm.byteCount
            //diff
            this[Filemods.insertcnt] = fm.diff.insert
            this[Filemods.deletecnt] = fm.diff.delete
            this[Filemods.replacecnt] = fm.diff.replace
        }
        print("#")
    }

    fun insertUsers(revWalk: Iterable<RevCommit>) {
        SchemaUtils.create(Users)

        //user insert
        val users = userSet(revWalk)
        Users.batchInsert(users) { user ->
            this[Users.id] = user.hashCode()
            this[Users.name] = user.name
            this[Users.email] = user.email
        }
        println("users ... done")
    }

    private fun userSet(revWalk: Iterable<RevCommit>): Set<MyUser> {
        //Create set of users (authors and commiters)
        val userMutableSet: MutableSet<MyUser> = mutableSetOf()
        for (commit in revWalk) {
            userMutableSet.add(getUser(commit.authorIdent))
            userMutableSet.add(getUser(commit.committerIdent))
        }
        return userMutableSet
    }

    private fun getUser(person: PersonIdent): MyUser {
        return MyUser(person.name, person.emailAddress)
    }

    private fun getUserId(person: PersonIdent): Int {
        return getUser(person).hashCode()
    }

    fun insertNotes(notes: Iterable<MyNote>) {
        SchemaUtils.create(Notes)

        Notes.batchInsert(notes) { nt ->
            this[Notes.commitid] = nt.commitId
            this[Notes.note] = nt.note
        }
    }
}