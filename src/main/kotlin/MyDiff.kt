package git2pg_kt

interface MyDiff {
    val insert: Int
    val delete: Int
    val replace: Int
}