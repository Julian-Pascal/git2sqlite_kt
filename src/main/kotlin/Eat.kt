package git2pg_kt

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.internal.storage.file.FileRepository
import org.eclipse.jgit.api.ListBranchCommand
import org.eclipse.jgit.errors.IncorrectObjectTypeException
import org.eclipse.jgit.lib.Ref
import org.eclipse.jgit.notes.Note
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.revwalk.filter.RevFilter
import git2pg_kt.CommitInfoExt as cf
import kotlin.streams.toList

object Eat {
    fun eat(git: Git, commits: List<RevCommit>): List<MyModInfo> {
        val commitStream = commits.parallelStream()
        //println(commitList.size)
        val cL = commitStream.map { commit ->
            cf.changedFilesWithDiff(git, commit)?.toList()
        }
        return cL.toList().filterNotNull().flatten()
    }

    fun commitList(git: Git): List<RevCommit> {
        //toList(): to iterate multiple times without reset
        return git.log().all().setRevFilter(RevFilter.NO_MERGES).call().toList()
    }

    /**
     * inspired by: https://stackoverflow.com/a/36477347
     * heavily modified, almost no similarities remaining
     */
    fun branchLookup(git: Git, repo: FileRepository): Sequence<List<MyBranchCommit>> {
        val branches = git.branchList().setListMode(ListBranchCommand.ListMode.ALL).call().asSequence()
        val result = branches.map { b -> branchNameAndCommits(b,repo, git)}
        //git.close()
        return result
    }
    private fun branchCommits(branch: Ref, repo: FileRepository, git: Git) : Iterable<RevCommit> {
        //return git.log().add(repo.resolve(branch.name)).call()
        return git.log().add(branch.objectId).setRevFilter(RevFilter.NO_MERGES).call()
    }
    private fun branchNameAndCommits(branch: Ref, repo: FileRepository, git: Git) : List<MyBranchCommit> {
        val res = branchCommits(branch, repo, git).map { commit ->
            MyBranchCommit(branch.name, commit.name)
        }
        return res
    }

    fun tagSet(git: Git): Set<MyTag> {
        val tagList = git.tagList().call().filterNotNull()
        val walk = RevWalk(git.repository)
        return tagList.map { tagMeta(walk, tagRef = it) }.toSet()
    }

    private fun tagMeta(walk: RevWalk, tagRef: Ref): MyTag {
        try {
            val tag = walk.parseTag(tagRef.objectId)
            return MyTag(
                commitId = tagRef.peeledObjectId.name,
                tagName = tag.tagName,
                tagMessage = tag.fullMessage
            )
        } catch (e: IncorrectObjectTypeException) {
            return MyTag(
                commitId = tagRef.objectId.name,
                tagName = tagRef.name.substring(10),
                tagMessage = ""
            )
        }
    }

    fun notesList(git: Git): List<MyNote> {
        val notesList = git.notesList().call().filterNotNull()
        return notesList.map { note: Note ->
            MyNote(note.name , String(git.repository.open(note.data).bytes))
        }
    }
}