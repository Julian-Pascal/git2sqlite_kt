package git2pg_kt

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.internal.storage.file.FileRepository
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.transactions.TransactionManager
import java.sql.Connection
import kotlin.math.roundToInt
import kotlin.math.roundToLong


fun main(args: Array<String>) {
    //Bsp: "project-name" "path"
    val dbName = args[0]
    val gitPath = args[1]
    val gitDir = "$gitPath/$dbName.git"

    println("extracting from $gitDir")

    Database.connect("jdbc:sqlite:db_sqlite/${dbName}_mirror.sqlite3", "org.sqlite.JDBC")
    TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE // Or Connection.TRANSACTION_READ_UNCOMMITTED

    println("starting extraction timer")
    val start = System.currentTimeMillis()

    val fileRepo = FileRepository(gitDir)
    val git = Git(fileRepo)

    println("#######################################################################################################")
///*
    println("start time")
    val commitList = Eat.commitList(git)

    println("commitList")
    transaction { Extract.insertUsers(commitList) }
    transaction { Extract.insertCommits(commitList) }
    transaction { Extract.insertParents(commitList) }
    println("commitList ..done")
//*/
    println("branches")
    Eat.branchLookup(git, fileRepo).forEach { branch ->
        print(".")
        transaction { Extract.insertBranch(branch) }
    }
    println("branches ..done")

    println("mods")
    commitList.chunked(2000).forEach { c ->
        val modList = Eat.eat(git,c)
        transaction { Extract.insertFilemods(modList) }
        print(".")
    }
    println("mods ..done")

    println("tags")
    val tags = Eat.tagSet(git)
    transaction { Extract.insertTags(tags) }
    println("tags ..done")

    println("notes")
    val notes = Eat.notesList(git)
    transaction { Extract.insertNotes(notes) }
    println("notes ..done")

    val stop = System.currentTimeMillis() - start
    val stopSec = (stop / 1000.0).roundToLong()
    println("extraction time for $dbName: $stop ms or ~ $stopSec s")
}

