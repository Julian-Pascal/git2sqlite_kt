import git2pg_kt.Eat
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.internal.storage.file.FileRepository
import org.eclipse.jgit.revwalk.RevCommit
import git2pg_kt.CommitInfoExt as cf

internal class CommitInfoExtTest {
    val gitDir = "./.git"
    val gitDir1 = "../git2pg/.git"
    val fileRepo = FileRepository(gitDir1)
    val git = Git(fileRepo)

    @org.junit.jupiter.api.Test
    fun myDiff() {
    }

    @org.junit.jupiter.api.Test
    fun modificationInfo() {
    }

    @org.junit.jupiter.api.Test
    fun changedFilesWithDiff() {
    }

    @org.junit.jupiter.api.Test
    fun diffInfo() {
    }

    @org.junit.jupiter.api.Test
    fun modType() {
    }

    @org.junit.jupiter.api.Test
    fun editListInfo() {
    }

    @org.junit.jupiter.api.Test
    fun editInfo() {
    }

    @org.junit.jupiter.api.Test
    fun addEditInfo() {
    }

    @org.junit.jupiter.api.Test
    fun linecntOfFileFromCommit() {
        val commits = Eat.commitList(git)
        val myCommit = commits.elementAt(1)
        println(myCommit)
        val pathName = cf.changedFiles(git, myCommit)!!.first()!!
        println(pathName)
        val lineCount = cf.linecntOfFileFromCommit(git, myCommit, pathName)
        println(lineCount)
        val byteCount = cf.bytecntOfFileFromCommit(git, myCommit, pathName)
        println(byteCount)
    }

    @org.junit.jupiter.api.Test
    fun bytecntOfFileFromCommit() {
    }

    @org.junit.jupiter.api.Test
    fun diffFormatterForChanges() {
    }

    @org.junit.jupiter.api.Test
    fun changedFiles() {
        val commitList = Eat.commitList(git)
        val cf1 = cf.changedFiles(git, commitList.elementAt(0))
        val cf2 = cf.changedFiles(git, commitList.elementAt(1))
        println("cf1: $cf1")
        println("cf2: $cf2")
        commitList.map { commit: RevCommit ->
            println(cf.changedFiles(git, commit))
        }
    }
}