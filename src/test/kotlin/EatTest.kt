import git2pg_kt.Eat as testedClass
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.internal.storage.file.FileRepository
import org.junit.jupiter.api.Test

import kotlin.system.measureTimeMillis

internal class EatTest {
    val gitDir = "./.git"
    val gitDir1 = "../git2pg/.git"
    val gitDir2 = "../jgit/.git"
    @Test
    fun eat() {
        val fileRepo = FileRepository(gitDir2)
        val git = Git(fileRepo)
        val commits = testedClass.commitList(git)
        val time = measureTimeMillis { testedClass.eat(git,commits) }
        /*val food = testedClass.eat(git)
        println(food.size)
        food.forEach {
            println(it)
        }*/
        println("time: $time")
    }

    @Test
    fun commitList() {
        val fileRepo = FileRepository(gitDir1)
        val git = Git(fileRepo)
        val commits = testedClass.commitList(git)
        commits.forEach { c ->
            println(c)
        }
    }

    @Test
    fun tagList() {
        val fileRepo = FileRepository(gitDir2)
        val git = Git(fileRepo)
        val tags = testedClass.tagSet(git)
        tags.forEach { tag ->
            println("${tag.commitId}       ${tag.tagName}")
        }
    }

    @Test
    fun notesList() {
        val fileRepo = FileRepository(gitDir)
        val git = Git(fileRepo)
        val notes = testedClass.notesList(git)
        notes.forEach { note ->
            println("${note.commitId}       ${note.note}")
        }
    }
}